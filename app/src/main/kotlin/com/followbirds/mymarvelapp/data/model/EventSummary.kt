package com.followbirds.mymarvelapp.data.model

data class EventSummary(
        val resourceURI: String?,
        val name: String?
)
