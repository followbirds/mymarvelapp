package com.followbirds.mymarvelapp.data.model

data class SeriesSummary(
        val resourceURI: String?,
        val name: String?
)