package com.followbirds.mymarvelapp.data.model

data class Image(
        val path: String?,
        val extension: String?
)
