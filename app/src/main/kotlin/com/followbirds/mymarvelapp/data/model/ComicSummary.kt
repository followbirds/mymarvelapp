package com.followbirds.mymarvelapp.data.model

data class ComicSummary(
        val resourceURI: String?,
        val name: String?
)
