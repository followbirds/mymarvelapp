package com.followbirds.mymarvelapp.data.model

data class StorySummary(
        val resourceURI: String?,
        val name: String?,
        val type: String?
)
