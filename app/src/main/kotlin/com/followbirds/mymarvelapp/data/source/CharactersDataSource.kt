package com.followbirds.mymarvelapp.data.source

import com.followbirds.mymarvelapp.data.model.CharacterDataWrapper
import io.reactivex.Single

interface CharactersDataSource {
    fun getCharacters(offset: Int, limit: Int): Single<CharacterDataWrapper>
    fun getCharacters(nameStartsWith: String): Single<CharacterDataWrapper>
}