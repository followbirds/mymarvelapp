package com.followbirds.mymarvelapp.data.model

data class Url(val type: String, val url: String)
