package com.followbirds.mymarvelapp.characters.usecase

import com.followbirds.mymarvelapp.UseCase
import com.followbirds.mymarvelapp.characters.filter.CharacterFilter
import com.followbirds.mymarvelapp.data.source.CharactersRepository
import com.followbirds.mymarvelapp.data.model.Character
import io.reactivex.Single

class GetCharacters(
        private val charactersRepository: CharactersRepository,
        private val filter: CharacterFilter) : UseCase<GetCharacters.Request, GetCharacters.Response> {

    override fun execute(request: Request): Response {
        val rxResponse = charactersRepository.getCharacters(request.offset, request.limit)
        return Response(rxResponse.map { it -> it.data?.results?.let { filter.filter(it) } })
    }

    class Request(val offset: Int, val limit: Int) : UseCase.RequestValues

    class Response(val characters: Single<List<Character>>) : UseCase.ResponseValue
}