package com.followbirds.mymarvelapp.characters.filter

import com.followbirds.mymarvelapp.data.model.Character

class EmptyFilter : CharacterFilter {
    override fun filter(characters: List<Character>): List<Character> {
        return characters
    }
}