package com.followbirds.mymarvelapp.characters.filter

import com.followbirds.mymarvelapp.data.model.Character

interface CharacterFilter {
    fun filter(characters: List<Character>) : List<Character>
}
