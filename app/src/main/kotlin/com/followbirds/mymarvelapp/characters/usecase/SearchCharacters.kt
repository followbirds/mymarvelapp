package com.followbirds.mymarvelapp.characters.usecase

import com.followbirds.mymarvelapp.UseCase
import com.followbirds.mymarvelapp.characters.filter.CharacterFilter
import com.followbirds.mymarvelapp.data.source.CharactersRepository
import com.followbirds.mymarvelapp.data.model.Character
import io.reactivex.Single


class SearchCharacters(
        private val charactersRepository: CharactersRepository,
        private val filter: CharacterFilter) : UseCase<SearchCharacters.Request, SearchCharacters.Response> {

    override fun execute(request: Request): Response {
        val rxResponse = charactersRepository.getCharacters(request.nameStartsWith)
        return Response(rxResponse.map { it -> it.data?.results?.let { filter.filter(it) } })
    }


    class Request(val nameStartsWith: String) : UseCase.RequestValues

    class Response(val characters: Single<List<Character>>) : UseCase.ResponseValue
}