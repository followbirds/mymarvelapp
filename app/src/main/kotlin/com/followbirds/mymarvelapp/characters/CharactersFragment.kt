package com.followbirds.mymarvelapp.characters

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.followbirds.mymarvelapp.AppNavigator
import com.followbirds.mymarvelapp.MyMarvelApplication
import com.followbirds.mymarvelapp.R
import com.followbirds.mymarvelapp.characters.adapter.CharacterSearchCursorAdapter
import com.followbirds.mymarvelapp.characters.adapter.CharactersAdapter
import com.followbirds.mymarvelapp.characters.filter.EmptyFilter
import com.followbirds.mymarvelapp.characters.filter.LimitFilter
import com.followbirds.mymarvelapp.characters.usecase.GetCharacters
import com.followbirds.mymarvelapp.characters.usecase.SearchCharacters
import com.followbirds.mymarvelapp.data.model.Character
import com.followbirds.mymarvelapp.data.model.Image
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit


class CharactersFragment : Fragment(), CharactersContract.View {

    @BindView(R.id.fragment_characters_list)
    lateinit var characters: RecyclerView

    @BindView(R.id.fragment_characters_loading)
    lateinit var mainCenteredProgressBar: ProgressBar

    @BindView(R.id.fragment_characters_more_loading)
    lateinit var smallBottomProgressBar: ProgressBar

    private lateinit var searchView: SearchView
    private lateinit var searchPublisher: PublishSubject<String>
    private lateinit var searchItemIdPublisher: PublishSubject<Int>

    private lateinit var presenter: CharactersContract.Presenter
    private lateinit var viewUnBinder: Unbinder
    private lateinit var adapter: CharactersAdapter
    private lateinit var searchSuggestionsAdapter: CharacterSearchCursorAdapter
    private var navigator: AppNavigator? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            navigator = context as AppNavigator
        } catch (e: ClassCastException) {
            Timber.e(e, "Parent activity must implement ${AppNavigator::class.java.name}")
        }
    }

    override fun onDetach() {
        navigator = null
        super.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_characters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewUnBinder = ButterKnife.bind(this, view)
        adapter = CharactersAdapter()
        searchSuggestionsAdapter = CharacterSearchCursorAdapter(context!!)
        characters.adapter = adapter
        characters.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        searchPublisher = PublishSubject.create()
        searchItemIdPublisher = PublishSubject.create()
        attachPresenter()
    }

    override fun onDestroyView() {
        searchView.setOnSuggestionListener(null)
        searchView.setOnQueryTextListener(null)
        viewUnBinder.unbind()
        super.onDestroyView()
    }

    private fun attachPresenter() {
        // fetch and build dependencies
        val charRepo = (activity?.application as MyMarvelApplication).charactersRepository
        val getCharactersUseCase = GetCharacters(charRepo, EmptyFilter())
        val searchCharactersUseCase = SearchCharacters(charRepo, LimitFilter(SEARCH_RESULT_LIMIT))
        // create presenter. it will attach itself to the receiving view (this)
        CharactersPresenter(view = this, getCharactersUseCase = getCharactersUseCase,
                searchCharactersUseCase = searchCharactersUseCase,
                ioScheduler = Schedulers.io(), viewScheduler = AndroidSchedulers.mainThread())
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_search, menu)
        val item = menu?.findItem(R.id.menu_item_search)
        searchView = item?.actionView as SearchView
        searchView.suggestionsAdapter = searchSuggestionsAdapter

        // RxSearchView bindings don't suffice
        searchView.setOnSuggestionListener(object : SearchView.OnSuggestionListener {
            override fun onSuggestionSelect(position: Int): Boolean = false

            override fun onSuggestionClick(position: Int): Boolean {
                searchItemIdPublisher.onNext(searchSuggestionsAdapter.getItemAt(position).id)
                return true
            }
        })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null && newText.length >= MINIMUM_CHARS_TO_SEARCH) {
                    searchPublisher.onNext(newText)
                } else {
                    hideSearchResults()
                }
                return true
            }
        })
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun onPause() {
        super.onPause()
        presenter.stop()
    }

    override fun setPresenter(presenter: CharactersContract.Presenter) {
        this.presenter = presenter
    }

    override fun showBigCenteredLoading() {
        mainCenteredProgressBar.visibility = View.VISIBLE
    }

    override fun showSmallBottomLoading() {
        smallBottomProgressBar.visibility = View.VISIBLE
        // TODO show/hide animation
    }

    override fun hideLoading() {
        mainCenteredProgressBar.visibility = View.GONE
        smallBottomProgressBar.visibility = View.GONE
    }

    override fun showCharacters(characters: List<Character>) {
        adapter.addCharacters(characters)
        Timber.d("Showing characters")
    }

    override fun searchedItemPressedWithId(): Observable<Int> {
        return searchItemIdPublisher
    }

    override fun searchedForTerm(): Observable<String> {
        return searchPublisher
    }

    private fun hideSearchResults() {
        Timber.d("Hiding search results")
        searchSuggestionsAdapter.setCharactes(emptyList())
    }

    override fun showSearchResult(characters: List<Character>) {
        Timber.d("Showing search results")
        searchSuggestionsAdapter.setCharactes(
                characters
                        .filter { character -> character.name != null && character.id != null && character.thumbnail != null }
                        .map { character ->
                            CharacterSearchCursorAdapter.SearchCharacter(getImageUrl(character.thumbnail!!),
                                    character.name!!,
                                    character.id!!)
                        })
    }

    private fun getImageUrl(image: Image): String = "${image.path}.${image.extension}"

    override fun scrolledToBottomWithOffset(): Observable<Int> {
        return adapter.reachedBottomWithOffset().debounce(400, TimeUnit.MILLISECONDS)
    }

    override fun characterSelected(): Observable<Character> {
        return adapter.characterSelected().debounce(400, TimeUnit.MILLISECONDS)
    }

    override fun showCharacterDetails(characterId: Int) {
        navigator?.navigateToCharacterDetailsView(characterId)
        Timber.d("showing character details (id=$characterId)")
    }

    companion object {
        private val SEARCH_RESULT_LIMIT = 5
        private val MINIMUM_CHARS_TO_SEARCH = 3

        fun newInstance(): CharactersFragment = CharactersFragment()
    }
}
