package com.followbirds.mymarvelapp.characters

import com.followbirds.mymarvelapp.BasePresenter
import com.followbirds.mymarvelapp.BaseView
import com.followbirds.mymarvelapp.data.model.Character
import io.reactivex.Observable

interface CharactersContract {
    interface View : BaseView<Presenter> {
        fun showBigCenteredLoading()
        fun showSmallBottomLoading()
        fun hideLoading()
        fun showCharacters(characters: List<Character>)
        fun characterSelected(): Observable<Character>
        fun showCharacterDetails(characterId: Int)
        fun scrolledToBottomWithOffset(): Observable<Int>
        fun searchedItemPressedWithId(): Observable<Int>
        fun searchedForTerm(): Observable<String>
        fun showSearchResult(characters: List<Character>)
    }

    interface Presenter : BasePresenter
}