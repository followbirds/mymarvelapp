package com.followbirds.mymarvelapp

interface AppNavigator {
    fun navigateToCharacterDetailsView(characterId: Int)
    fun navigateToCharactersView()
}
