package com.followbirds.mymarvelapp;

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import com.facebook.stetho.timber.StethoTree
import com.followbirds.mymarvelapp.util.log.CrashReportTree
import com.followbirds.mymarvelapp.data.source.CharactersRepository
import com.followbirds.mymarvelapp.data.source.remote.CharactersRemoteDataSource
import com.followbirds.mymarvelapp.data.source.remote.MarvelServiceFactory
import com.followbirds.mymarvelapp.util.NetworkStatus
import timber.log.Timber

class MyMarvelApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            initializeStetho(applicationContext)
        } else {
            Timber.plant(CrashReportTree())
        }

        createCharactersRepository(this)
    }

    private fun initializeStetho(context: Context) {
        val initializer = Stetho.newInitializerBuilder(context)
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(context))
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
                .build()

        Stetho.initialize(initializer)

        Timber.plant(StethoTree())
    }

    private fun createCharactersRepository(context: Context) {
        val marvelApi = MarvelServiceFactory.makeMarvelCharactersApi(BuildConfig.DEBUG,
                context.cacheDir, BuildConfig.CACHE_SIZE, NetworkStatus())
        val charactersRemoteDataSource = CharactersRemoteDataSource(marvelApi,
                BuildConfig.MARVEL_API_PUBLIC_KEY, BuildConfig.MARVEL_API_PRIVATE_KEY)
        charactersRepository = CharactersRepository(charactersRemoteDataSource)
    }

    lateinit var charactersRepository: CharactersRepository
}