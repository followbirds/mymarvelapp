package com.followbirds.mymarvelapp

interface BasePresenter {
    fun start()
    fun stop()
}
