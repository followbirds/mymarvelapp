package com.followbirds.mymarvelapp

interface BaseView<T : BasePresenter> {
    fun setPresenter(presenter: T)
}
